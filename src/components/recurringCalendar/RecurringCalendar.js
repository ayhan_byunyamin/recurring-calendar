import React, { Component } from 'react';
import RRuleGenerator from 'react-rrule-generator';
import 'react-rrule-generator/build/styles.css';
import 'bootstrap/dist/css/bootstrap.css';

class RecurringCalendar extends Component {
    state = {
        rrule: 'SOME REALLY COOL RRULE'
    }
    render() {
        return (
            <RRuleGenerator
                onChange={(rrule) => console.log(`RRule changed, now it's ${rrule}`)}
                config={{
                    repeat: ['Yearly', 'Monthly', 'Weekly', 'Daily'],
                    yearly: 'on',
                    monthly: 'on'
                }}
            />
        );
    }
}

export default RecurringCalendar;