import React, { Component } from 'react';
import BigCalendar from 'react-big-calendar-like-google'
import moment, { now } from 'moment'
import { RRule, RRuleSet, rrulestr } from 'rrule';
import { DateTime } from "luxon";
import 'react-big-calendar-like-google/lib/css/react-big-calendar.css';

class Calendar extends Component {
    state = {
        events: [
            {
                start: new Date(),
                end: new Date(moment().add(1, "days")),
                title: "Some title"
            },
            {
                start: new Date(),
                end: new Date(moment().add(1, "days")),
                title: "Some recurring title"
            }
        ],
        ocurrence: [
            '2019-02-07T10:30:00.000Z',
            '2019-02-08T10:30:00.000Z',
            '2019-03-09T10:30:00.000Z',
            '2019-04-09T10:30:00.000Z',
            '2019-04-13T10:30:00.000Z',
            '2019-05-14T10:30:00.000Z',
            '2019-05-18T10:30:00.000Z',
        ]
    }

    componentWillMount() {
        const date = this.setOcurrence();
        console.log('TCL: Calendar -> componentWillMount -> date', date)
        let events = [...this.state.events];
        let event = {
            start: date,
            end: new Date(moment().add(3, "days")),
            title: 'Formated Event'
        };
        console.log('TCL: Calendar -> componentWillMount -> event', event)
        events.push(event);
        this.setState({ events: events });
    }

    setOcurrence = () => {
        const jsDate = this.state.ocurrence[0];
        const luxor = DateTime.fromISO(jsDate).toUTC().toJSDate().toUTCString();
        return luxor;

    }
    render() {
        const localizer = BigCalendar.momentLocalizer(moment);
        /**
         * From ISO format /'2018-02-07T10:30:00.000Z'/ to UTC to JSDate NOTE: months in js are index based in luxor normal/people based!
         */
        const jsDate = this.state.ocurrence[0];
        const luxor = DateTime.fromISO(jsDate).toUTC().toJSDate().toUTCString();
        console.log('ToJSDate', luxor);
        console.log('moment', moment()._d.toUTCString());
        console.log('JS Date', this.state.events[0].start.toUTCString());

        return (
            <div style={{ width: "90%", height: "90vh", margin: "auto auto" }}>
                <BigCalendar
                    localizer={localizer}
                    defaultDate={new Date()}
                    defaultView="month"
                    events={this.state.events}
                    style={{ height: "90vh" }}
                    startAccessor="start"
                    endAccessor="end"
                />
            </div>
        );
    }
}

export default Calendar;