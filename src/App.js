import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Calendar from '../src/components/calendar';
import RecurringCalendar from './components/recurringCalendar/RecurringCalendar';
class App extends Component {
  render() {
    return (
      <div>
        <Route path="/calendar" component={Calendar} />
        <Route path="/recurring-calendar" component={RecurringCalendar} />
      </div>
    );
  }
}

export default App;
